var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.AdaptiveMedia = youbora.Adapter.extend({

  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.player.currentTime
  },

  /** Override to return current playrate */
  getPlayrate: function () {
    return this.player.playbackRate
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.player.duration
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    if (this.mediaSource) {
      return this.mediaSource.currentPlaybackBitrate
    }
    return null
  },

  /** Override to return user bandwidth throughput */
  getThroughput: function () {
    if (this.mediaSource) {
      return this.mediaSource.currentDownloadBitrate
    }
    return null
  },

  /** Override to return rendition */
  getRendition: function () {
    return youbora.Util.buildRenditionString(this.player.videoWidth, this.player.videoHeight, this.getBitrate())
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    if (this.mediaSource) {
      return this.mediaSource.isLive
    }
    return null
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.player.src || this.player.currentSrc
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    var p = Windows.ApplicationModel.Package.current.id.version
    return p.major + '.' + p.minor + '.' + p.build + '.' + p.revision
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'AdaptiveMedia-JS'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Console all events if logLevel=DEBUG
    youbora.Util.logAllEvents(this.player)

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, true)

    // References
    this.references = []
    this.references['loadstart'] = this.playListener.bind(this)
    this.references['play'] = this.playListener.bind(this)
    this.references['playing'] = this.playingListener.bind(this)
    this.references['timeupdate'] = this.timeupdateListener.bind(this)
    this.references['pause'] = this.pauseListener.bind(this)
    this.references['ended'] = this.endedListener.bind(this)
    this.references['error'] = this.errorListener.bind(this)
    this.references['seeking'] = this.seekingListener.bind(this)

    // Register listeners
    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      this.references = []
    }
  },

  setMediaSource: function (mediaSource) {
    this.mediaSource = mediaSource
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this.fireStart()
  },

  /** Listener for 'timeupdate' event. */
  timeupdateListener: function (e) {
    if (this.getPlayhead() > 0.1 && !this.flags.isJoined) {
      this.fireJoin()
      this.monitor.skipNextTick()
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    this.fireResume()
    this.fireSeekEnd()
    this.fireBufferEnd()
    if (!this.flags.isStarted) {
      this.fireStart()
    }
    if (!this.flags.isJoined) {
      this.fireJoin()
      this.monitor.skipNextTick()
    }
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    this.fireError()
  },

  /** Listener for 'seeking' event. */
  seekingListener: function (e) {
    this.fireSeekBegin()
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    this.fireStop()
  }
})

module.exports = youbora.adapters.AdaptiveMedia
